package net.ruduo.weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ruduo.weather.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import static net.ruduo.weather.Helpers.admin;
import static net.ruduo.weather.Helpers.toJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@RunWith(SpringRunner.class)
public class UserIntegrationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldNotAddUserWithoutPassword() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", null, "Adam", "Diesel");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldAddSimpleUser() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\"}"));
    }

    @Test
    public void shouldAddUserWithDetails() throws Exception {
        Map<String, Object> john = Helpers.getUserMap("john", "mypassword", "John", "Petrol", "Lithuania", Arrays.asList("Tokyo", "Norway"));
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(john))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("{\"username\":\"john\",\"firstName\":\"John\",\"lastName\":\"Petrol\", \"homeLocation\":\"Lithuania\", \"favoriteCities\":[{\"name\":\"Tokyo\"},{\"name\":\"Norway\"}]}"));
    }

    @Test
    public void shouldNotAddUserWithCaseSimilarUsername() throws Exception {
        Map<String, Object> john = Helpers.getUserMap("john", "mypassword", "John", "Petrol");

        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(john))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        Map<String, Object> john2 = Helpers.getUserMap("John", "mypassword", "John", "Petrol");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(john2))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldGetTwoUsersAndAdmin() throws Exception {
        Map<String, Object> john = Helpers.getUserMap("john", "mypassword", "John", "Petrol");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(john))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        mockMvc.perform(get("/api/v1/users")
                .with(admin()))
                .andExpect(status().isOk())
                .andExpect(content().json("[" +
                        "{\"username\":\"admin\"}" +
                        ",{\"username\":\"john\",\"firstName\":\"John\",\"lastName\":\"Petrol\"}" +
                        ",{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\"}" +
                        "]"));
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        adam.put("homeLocation", "Solar system");
        adam.put("favoriteCities", Arrays.asList("Tokyo", "Norway"));

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\",\"homeLocation\":\"Solar system\",\"favoriteCities\":[{\"name\":\"Tokyo\"},{\"name\":\"Norway\"}]}"));
    }

    @Test
    public void shouldNotUpdateUserWithNonExistingCity() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        adam.put("homeLocation", "Solar system");
        adam.put("favoriteCities", Arrays.asList("Tokyo", "Blabla"));

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()); // Need to discuss what should be output if users favorite city doesn't exist
    }

    @Test
    public void shouldUpdateUserCitiesWhenSameCityAlreadyExists() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel", "", Collections.singletonList("Tokyo"));
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        adam.put("homeLocation", "Solar system");
        adam.put("favoriteCities", Arrays.asList("Tokyo", "New York"));

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\",\"homeLocation\":\"Solar system\",\"favoriteCities\":[{\"name\":\"Tokyo\"}, {\"name\":\"New York\"}]}"));
    }

    @Test
    public void shouldUpdateUserCitiesWithEmpty() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel", "", Arrays.asList("Tokyo", "Kaunas"));
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        adam.put("favoriteCities", Collections.emptyList());

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\",\"homeLocation\":\"\",\"favoriteCities\":[]}"));
    }


    public void shouldFailUpdateUserWithCorrectUuidButChangedUsername() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        adam.put("username", "adam2");
        //adam = userService.setFavoriteCities(adam, Arrays.asList("Tokyo", "Norway"));

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    public void shouldFailUpdateUserWithWrongUuid() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");

        UUID addedUuid = UUID.randomUUID();

        mockMvc.perform(put("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldDeleteAddedUser() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        mockMvc.perform(delete("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldFailDeleteUserWithWrongUuid() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");

        UUID addedUuid = UUID.randomUUID();

        mockMvc.perform(delete("/api/v1/users/{uuid}", addedUuid)
                .with(admin())
                .content(toJson(adam))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldFindAddedUser() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        mockMvc.perform(get("/api/v1/users/?firstName=Ada")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\"}]"));
    }

    @Test
    public void shouldNotFindAddedUser() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel");
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        mockMvc.perform(get("/api/v1/users/?firstName=Kevin")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void shouldFindUserByCity() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel", "", Arrays.asList("Tokyo", "Kaunas"));
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        Map<String, Object> john = Helpers.getUserMap("john", "mypassword", "John", "Petrol", "", Arrays.asList("Vilnius", "New York"));
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(john))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/api/v1/users/?cityName=tokyo")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"username\":\"adam\",\"firstName\":\"Adam\",\"lastName\":\"Diesel\",\"homeLocation\":\"\",\"favoriteCities\":[{\"id\":598316,\"name\":\"Kaunas\",\"country\":\"LT\",\"coord\":{\"lon\":23.9,\"lat\":54.900002}},{\"id\":1850147,\"name\":\"Tokyo\",\"country\":\"JP\",\"coord\":{\"lon\":139.691711,\"lat\":35.689499}}]}]"));
    }

    @Test
    public void shouldNotFindUserByCity() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel", "", Arrays.asList("Tokyo", "Kaunas"));
        mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        mockMvc.perform(get("/api/v1/users/?cityName=Kevin")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void shouldGetBasicWeatherInfoForUserCities() throws Exception {
        Map<String, Object> adam = Helpers.getUserMap("adam", "something", "Adam", "Diesel", "", Arrays.asList("Kaunas", "Vilnius"));
        MvcResult mvcResult = mockMvc
                .perform(post("/api/v1/users")
                        .with(admin())
                        .content(toJson(adam))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        User userAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), User.class);
        UUID addedUuid = userAdded.getId();

        mockMvc.perform(get("/api/v1/users/{uuid}/weather/current", addedUuid)
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
