package net.ruduo.weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.web.CityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.Map;

import static net.ruduo.weather.Helpers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CityIntegrationTest {
    @Autowired
    CityService cityService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void addActiveCityWithAllArgs() throws Exception {
        Map<String, Object> input1 = getCityMap("Kaunas", 123456790, 999999999);
        this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(input1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("{\"id\":598316,\"name\":\"Kaunas\",\"country\":\"LT\",\"area\":123456790,\"population\":999999999,\"coord\":{\"lon\":23.9,\"lat\":54.900002}}"));
    }

    @Test
    public void addActiveCityWithOnlyName() throws Exception {
        Map<String, Object> input1 = getCityMap("Vilnius");
        this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(input1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("{\"id\":593116,\"name\":\"Vilnius\",\"country\":\"LT\",\"area\":0,\"population\":0,\"coord\":{\"lon\":25.2798,\"lat\":54.689159}}"));
    }


    @Test
    public void shouldGetListOfCities() throws Exception {
        Map<String, Object> city1 = getCityMap("Kaunas", 123456790, 999999999);
        this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(city1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        Map<String, Object> input1 = getCityMap("Vilnius");
        this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(input1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        this.mockMvc.perform(get("/api/v1/cities")
                .with(admin()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":593116,\"name\":\"Vilnius\",\"country\":\"LT\",\"area\":0,\"population\":0,\"coord\":{\"lon\":25.2798,\"lat\":54.689159}},{\"id\":598316,\"name\":\"Kaunas\",\"country\":\"LT\",\"area\":123456790,\"population\":999999999,\"coord\":{\"lon\":23.9,\"lat\":54.900002}}]"));
    }

    @Test
    public void deleteActiveCity() throws Exception {
        Map<String, Object> input1 = getCityMap("Vilnius");
        MvcResult mvcResult = this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(input1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        City cityAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), City.class);
        long id = cityAdded.getId();

        mockMvc.perform(delete("/api/v1/cities/{id}", id)
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldUpdateActiveCity() throws Exception {
        Map<String, Object> city1 = getCityMap("Vilnius");
        MvcResult mvcResult = this.mockMvc.perform(post("/api/v1/cities")
                .with(admin())
                .content(toJson(city1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        City cityAdded = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), City.class);
        long id = cityAdded.getId();
        Map<String, Object> city2 = getCityMap("Vilnius", 500, 200);
        mockMvc.perform(put("/api/v1/cities/{id}", id)
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(city2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":593116,\"name\":\"Vilnius\",\"country\":\"LT\",\"area\":500,\"population\":200,\"coord\":{\"lon\":25.2798,\"lat\":54.689159}}"));
    }

    @Test
    public void shouldNotUpdateNotExistingCity() throws Exception {
        long id = 123456;
        Map<String, Object> city2 = getCityMap("Vilnius", 500, 200);
        mockMvc.perform(put("/api/v1/cities/{id}", id)
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(city2)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldGetWeatherForecastForCity() throws Exception {
        this.mockMvc.perform(get("/api/v1/cities/{city}/weather/days/5", "Vilnius")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("{\"City(id=593116, name=Vilnius, country=LT, area=0, population=0, coord={lon=25.2798, lat=54.689159})\":[{},{},{},{},{}]}"));
    }

    @Test
    public void shouldGetWeatherForecastForCityByCoords() throws Exception {
        this.mockMvc.perform(get("/api/v1/cities/coords/weather/days/3?latitude=38.01&longitude=23.65")
                .with(admin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("{\"City(id=260183, name=Chaidari, country=GR, area=0, population=0, coord={lon=23.66667, lat=38.01667})\":[{},{},{}]}"));
    }

    // Should be more test cases... and I know that
}