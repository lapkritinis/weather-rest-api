package net.ruduo.weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

class Helpers {
    static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static Map<String, Object> getUserMap(String username, String password, String firstName, String lastName) {
        Map<String, Object> user = new HashMap<>();
        user.put("username", username);
        user.put("password", password);
        user.put("firstName", firstName);
        user.put("lastName", lastName);
        return user;
    }

    static Map<String, Object> getUserMap(String username, String password, String firstName, String lastName, String homeLocation, List<String> favoriteCities) {
        Map<String, Object> user = getUserMap(username, password, firstName, lastName);
        user.put("homeLocation", homeLocation);
        user.put("favoriteCities", favoriteCities);
        return user;
    }

    static Map<String, Object> getCityMap(String name) {
        Map<String, Object> city = new HashMap<>();
        city.put("name", name);
        return city;
    }

    static Map<String, Object> getCityMap(String name, long area, long population) {
        Map<String, Object> city = new HashMap<>();
        city.put("name", name);
        city.put("area", area);
        city.put("population", population);
        return city;
    }

    static RequestPostProcessor admin() {
        return httpBasic("admin", "password");
    }
}
