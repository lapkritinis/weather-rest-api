package net.ruduo.weather;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class BasicAuthenticationIntegrationTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void accessWithInvalidCredentials() throws Exception {
        this.mockMvc
                .perform(get("/api/v1/users").with(httpBasic("admin", "wrong")))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void accessWithValidCredentials() throws Exception {
        this.mockMvc
                .perform(get("/api/v1/users").with(httpBasic("admin", "password")))
                .andExpect(status().isOk());
    }
}
