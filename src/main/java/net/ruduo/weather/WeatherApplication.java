package net.ruduo.weather;

import lombok.extern.slf4j.Slf4j;
import net.ruduo.weather.entity.User;
import net.ruduo.weather.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class WeatherApplication {
    private final BCryptPasswordEncoder passwordEncoder;

    private final UserRepo userRepo;

    @Autowired
    public WeatherApplication(BCryptPasswordEncoder passwordEncoder, UserRepo userRepo) {
        this.passwordEncoder = passwordEncoder;
        this.userRepo = userRepo;
    }

    public static void main(String[] args) {
        SpringApplication.run(WeatherApplication.class, args);
    }

    @PostConstruct
    public void init() {
        // Adding admin user
        User user = new User("admin", passwordEncoder.encode("password"), "Admin", "");

        if (!userRepo.findFirstByUsernameIgnoreCase(user.getUsername()).isPresent()) {
            userRepo.save(user);
        }
        log.info("Added admin user with default password.");
    }
}
