package net.ruduo.weather.resource.execptions;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Slf4j
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class ResourceExistsException extends RuntimeException {
    private final String resourceName;
    private final String fieldName;
    private final Object fieldValue;

    public ResourceExistsException(String resourceName, String fieldName, Object fieldValue) {
        super(String.format("%s exists %s : '%s'", resourceName, fieldName, fieldValue));
        log.info("Resource exists");
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
