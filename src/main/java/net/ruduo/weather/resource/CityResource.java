package net.ruduo.weather.resource;

import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThan;
import net.kaczmarzyk.spring.data.jpa.domain.LessThan;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Conjunction;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.model.WeatherForecast;
import net.ruduo.weather.resource.execptions.ResourceNotFoundException;
import net.ruduo.weather.web.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
        path = "/api/v1/cities"
)
@Slf4j
public class CityResource {
    private final CityService cityService;

    @Autowired
    public CityResource(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            path = "search/"
    )
    public List<City> filterByArea(
            @Conjunction(value = {
                    @Or({
                            @Spec(path = "population", params = "populationGreater", spec = GreaterThan.class),
                            @Spec(path = "population", params = "populationLess", spec = LessThan.class)
                    }),
                    @Or({
                            @Spec(path = "area", params = "areaGreater", spec = GreaterThan.class),
                            @Spec(path = "area", params = "areaLess", spec = LessThan.class)
                    }),
                    @Or({
                            @Spec(path = "country", spec = LikeIgnoreCase.class)
                    })
            })
                    Specification<City> spec) {
        return cityService.getCities(spec);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<City> getCities() {
        return cityService.getCities();
    }

    @GetMapping(
            value = "/coords/weather/days/{period}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Map<City, List<WeatherForecast>> getCityWeatherExtendedForecastByDays(@RequestParam double latitude, @RequestParam double longitude, @PathVariable long period) {
        City closestCityByCoords = cityService.getClosestCityByCoords(latitude, longitude);
        return cityService.getWeatherExtendedForecast(closestCityByCoords, ChronoUnit.DAYS, period);
    }

    @GetMapping(
            value = "/coords/weather/hours/{period}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Map<City, List<WeatherForecast>> getCityWeatherExtendedForecastByHours(@RequestParam double latitude, @RequestParam double longitude, @PathVariable long period) {
        City closestCityByCoords = cityService.getClosestCityByCoords(latitude, longitude);
        return cityService.getWeatherExtendedForecast(closestCityByCoords, ChronoUnit.HOURS, period);
    }

    @GetMapping(
            value = "/{city}/weather/days/{period}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Map<City, List<WeatherForecast>> getCityWeatherExtendedForecastByDays(@PathVariable String city, @PathVariable long period) {
        return cityService.getWeatherExtendedForecast(city, ChronoUnit.DAYS, period);
    }

    @GetMapping(
            value = "/{city}/weather/hours/{period}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Map<City, List<WeatherForecast>> getCityWeatherExtendedForecastByHours(@PathVariable String city, @PathVariable long period) {
        return cityService.getWeatherExtendedForecast(city, ChronoUnit.HOURS, period);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public City addCity(@RequestBody City input) {
        return cityService.addCity(input);
    }

    @DeleteMapping(
            value = "/{id}"
    )
    public ResponseEntity deleteCity(@PathVariable("id") Long id) {
        if (cityService.deleteCity(id)) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        throw new ResourceNotFoundException("City", "id", id);
    }

    @PutMapping(
            value = "/{id}"
    )
    public ResponseEntity updateCityById(@PathVariable("id") long id, @RequestBody City input) {
        City cityUpdated = cityService.updateCity(id, input);
        return new ResponseEntity<>(cityUpdated, HttpStatus.OK); // Possible to change response code according to requirement
    }
}
