package net.ruduo.weather.resource;

import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.entity.User;
import net.ruduo.weather.model.WeatherCurrent;
import net.ruduo.weather.resource.execptions.ResourceExistsException;
import net.ruduo.weather.resource.execptions.ResourceNotFoundException;
import net.ruduo.weather.web.UserService;
import net.ruduo.weather.web.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(
        path = "/api/v1/users"
)

public class UserResource {
    private final UserService userService;
    private final WeatherService weatherService;

    @Autowired
    public UserResource(UserService userService, WeatherService weatherService) {
        this.userService = userService;
        this.weatherService = weatherService;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<User> filterUsersByFirstName(
            @Or({
                    @Spec(path = "firstName", spec = LikeIgnoreCase.class),
                    @Spec(path = "lastName", spec = LikeIgnoreCase.class)
            })
                    Specification<User> spec) {
        return userService.findAll(spec);
    }


    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/{uuid}"
    )
    public User fetchUser(@PathVariable("uuid") UUID uuid) {
        return userService.getUser(uuid).orElseThrow(() -> new ResourceNotFoundException("User", "uuid", uuid));
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = "cityName"
    )
    public List<User> filterUsersByCity(
            @Join(path = "favoriteCities", alias = "c")
            @Spec(path = "c.name", params = "cityName", spec = LikeIgnoreCase.class)
                    Specification<User> spec) {

        return userService.findAll(spec);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<User> addUser(@RequestBody @Valid User user) {
        User userAdded = userService.addUser(user).orElseThrow(() -> new ResourceExistsException("User", "username", user.getUsername()));
        return new ResponseEntity<>(userAdded, HttpStatus.CREATED);
    }

    @DeleteMapping(
            value = "/{uuid}"
    )
    public ResponseEntity deleteUserByUuid(@PathVariable("uuid") UUID uuid) {
        if (userService.deleteUser(uuid)) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        throw new ResourceNotFoundException("User", "uuid", uuid.toString());
    }

    @PutMapping(
            value = "/{uuid}"
    )
    public ResponseEntity<User> updateUserByUuid(@PathVariable("uuid") UUID uuid, @RequestBody @Valid User user) {
        User userUpdated = userService.updateUser(uuid, user).orElseThrow(() -> new ResourceNotFoundException("User", "uuid and username", uuid.toString() + " " + user.getUsername()));
        return new ResponseEntity<>(userUpdated, HttpStatus.OK); // Possible to change response code according to requirement
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/{uuid}/weather/current"
    )
    public Map<City, WeatherCurrent> get(@PathVariable("uuid") UUID uuid) {
        User user = userService.getUser(uuid).orElseThrow(() -> new ResourceNotFoundException("User", "uuid", uuid));
        return weatherService.getBasicCurrentInfo(user.getFavoriteCities());
    }
}
