package net.ruduo.weather.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.model.WeatherForecast;
import net.ruduo.weather.repo.CityRepo;
import net.ruduo.weather.resource.execptions.ResourceExistsException;
import net.ruduo.weather.resource.execptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

@Slf4j
@Service
@Transactional
public class CityService {
    private final CityRepo cityRepo;
    private final WeatherService weatherService;
    private final List<City> allCities = loadAllCities();

    @Autowired
    public CityService(CityRepo cityRepo, WeatherServiceFake weather) {
        this.cityRepo = cityRepo;
        this.weatherService = weather;
    }

    private List<City> loadAllCities() {
        log.info("Loading all favoriteCities list from JSON.");

        try (
                // Using saved file to speed up things, but in real world I would use URL.
                InputStream inputStream = CityService.class.getClassLoader().getResourceAsStream("city.list.json.gz");
                GZIPInputStream gzis = new GZIPInputStream(new BufferedInputStream(inputStream, 65535))
        ) {
            ObjectMapper mapper = new ObjectMapper();
            List<City> cities = mapper.readValue(gzis, new TypeReference<List<City>>() {
            });
            log.info("Done loading all favoriteCities from JSON. Loaded {} favoriteCities.", cities.size());
            return cities;
        } catch (IOException e) {
            // If fails returns empty list. New favoriteCities wont be possible to insert, but application will remain functional.
            log.error("Failed loading all favoriteCities from JSON.");
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<City> getCities() {
        return cityRepo.findAll();
    }

    public List<City> getCities(Specification<City> spec) {
        return cityRepo.findAll(spec);
    }

    public City addCity(City inputCity) {
        log.info("Adding new city: {}", inputCity);

        City city = getCityIdFromJson(inputCity.getName());

        if (cityExists(city.getId())) {
            throw new ResourceExistsException("City", "name", inputCity.getName());
        }
        updateCityFromInput(inputCity, city);
        cityRepo.save(city);
        log.info("Added new city: {}", city);
        return city;
    }

    public boolean deleteCity(long id) {
        log.info("Deleting city with id: {}", id);

        if (cityExists(id)) {
            cityRepo.deleteById(id);
            log.info("Deleted city with id: {}", id);
            return true;
        }
        return false;
    }

    public void updateCity(City input) { // Used to add cities when User updates its favorite cities
        log.info("Updating city: {}", input);
        City city = getCityIdFromJson(input.getName());
        cityRepo.save(city);
        log.info("Updated city: {}", city);
    }

    public City updateCity(long id, City input) {
        log.info("Updating city: {}", input);
        City city = cityRepo.findFirstById(id).orElseThrow(() -> new ResourceNotFoundException("City", "id", id));
        updateCityFromInput(input, city);
        cityRepo.save(city);
        log.info("Updated city: {}", city);
        return city;
    }

    private void updateCityFromInput(City input, City city) {
        city.setArea(input.getArea());
        city.setPopulation(input.getPopulation());
    }

    private boolean cityExists(long id) {
        return cityRepo.findById(id).isPresent();
    }

    public Map<City, List<WeatherForecast>> getWeatherExtendedForecast(String city, ChronoUnit chronoUnit, long period) {
        return weatherService.getExtendedForecastByCity(getCityIdFromJson(city), chronoUnit, period);
    }

    public Map<City, List<WeatherForecast>> getWeatherExtendedForecast(City city, ChronoUnit chronoUnit, long period) {
        return weatherService.getExtendedForecastByCity(city, chronoUnit, period);
    }

    public City getCityIdFromJson(String name) {
        return allCities.stream().filter(city -> city.getName().equalsIgnoreCase(name)).findFirst().orElseThrow(() -> new ResourceNotFoundException("City", "name", name));
    }

    public City getClosestCityByCoords(double latitude, double longitude) { // first goes latitude
        // Linear search due small amount of data. https://en.wikipedia.org/wiki/Nearest_neighbor_search
        if (latitude < -90 || latitude > 90) throw new IllegalArgumentException("Latitude must be in range -90...90");
        if (longitude < -180 || longitude > 180)
            throw new IllegalArgumentException("Longitude must be in range -180...180");
        final City[] found = new City[1];
        final Double[] distance = {Double.MAX_VALUE};
        allCities.forEach(city -> {
            Map<String, Double> coord = city.getCoord();
            Double latitude1 = coord.get("lat");
            Double longitude1 = coord.get("lon");
            Double calcDistance = Math.sqrt(Math.pow(latitude1 - latitude, 2) + Math.pow(longitude1 - longitude, 2));
            if (distance[0] > calcDistance) {
                found[0] = city;
                distance[0] = calcDistance;
            }
        });
        return found[0];
    }
}

