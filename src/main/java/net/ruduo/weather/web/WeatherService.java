package net.ruduo.weather.web;

import net.ruduo.weather.entity.City;
import net.ruduo.weather.model.WeatherCurrent;
import net.ruduo.weather.model.WeatherForecast;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public interface WeatherService {
    // basic current weather information (min and max temperature, weather conditions)
    Map<City, WeatherCurrent> getBasicCurrentInfo(Set<City> cities);

    // single city with extended current weather information (temperatures, weather conditions, pressure, humidity, etc)
    Map<City, List<WeatherForecast>> getExtendedForecastByCity(City city, ChronoUnit timeUnit, long period);
}
