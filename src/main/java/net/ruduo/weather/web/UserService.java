package net.ruduo.weather.web;

import lombok.extern.slf4j.Slf4j;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.entity.User;
import net.ruduo.weather.repo.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Service
@Transactional
public class UserService implements UserDetailsService {
    private final CityService cityService;
    private final UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo, CityService cityService) {
        this.userRepo = userRepo;
        this.cityService = cityService;
    }

    public Optional<User> addUser(User user) {
        Set<City> favoriteCitiesInput = user.getFavoriteCities();
        favoriteCitiesInput.forEach(cityService::addCity);

        log.info("Adding new user: {}", user);
        if (!userRepo.findFirstByUsernameIgnoreCase(user.getUsername()).isPresent()) {
            User addedUser = userRepo.saveAndFlush(user);
            log.info("Added new user: {}", user);
            return Optional.of(addedUser);
        }
        return Optional.empty();
    }

    public boolean deleteUser(UUID uuid) {
        log.info("Deleting user by UUID: {}", uuid);
        if (userRepo.findFirstById(uuid).isPresent()) {
            userRepo.deleteUserById(uuid);
            log.info("Deleted user by UUID: {}", uuid);
            return true;
        }
        return false;
    }

    public Optional<User> updateUser(UUID uuid, User userUpdated) {
        log.info("Updating user by UUID: {}", uuid);
        Optional<User> userCurrent = userRepo.findFirstById(uuid);
        if (userCurrent.isPresent()) {
            User user = userCurrent.get();
            // Do not allow change username
            if (userUpdated.getUsername().equalsIgnoreCase(user.getUsername())) {
                userUpdated.getFavoriteCities().forEach(cityService::updateCity);
                userUpdated.setId(uuid);
                BeanUtils.copyProperties(userUpdated, user);
                userRepo.save(user);
                log.info("Updated user by UUID: {}, User: {}", uuid, user);
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findFirstByUsernameIgnoreCase(username).orElseThrow(() ->
                new UsernameNotFoundException("Invalid username or password."));

        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("FAKE_ROLE")));
    }

    public Optional<User> getUser(UUID uuid) {
        return userRepo.findFirstById(uuid);
    }

    public List<User> findAll(Specification<User> customerSpec) {
        return userRepo.findAll(customerSpec);
    }
}
