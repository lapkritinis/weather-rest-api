package net.ruduo.weather.web;

import net.ruduo.weather.entity.City;
import net.ruduo.weather.model.WeatherCurrent;
import net.ruduo.weather.model.WeatherForecast;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Component
public class WeatherServiceFake implements WeatherService {
    @Override
    public Map<City, WeatherCurrent> getBasicCurrentInfo(Set<City> cities) {
        Map<City, WeatherCurrent> result = new LinkedHashMap<>();
        cities.forEach(city -> result.put(city, new WeatherCurrent()));
        return result;
    }

    @Override
    public Map<City, List<WeatherForecast>> getExtendedForecastByCity(City city, ChronoUnit chronoUnit, long period) {
        List<WeatherForecast> weatherForecasts = new ArrayList<>();
        LocalDate date = LocalDate.now();
        LocalDateTime time = LocalDateTime.now();
        for (long i = 1; i <= period; i++) {
            if (chronoUnit.equals(ChronoUnit.DAYS)) {
                weatherForecasts.add(new WeatherForecast(date.plus(i, chronoUnit)));
            } else {
                weatherForecasts.add(new WeatherForecast(time.plus(i, chronoUnit)));
            }
        }
        Map<City, List<WeatherForecast>> forecast = new LinkedHashMap<>();
        forecast.put(city, weatherForecasts);
        return forecast;
    }
}
