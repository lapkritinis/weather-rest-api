package net.ruduo.weather.repo;

import net.ruduo.weather.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface CityRepo extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {
    Optional<City> findFirstById(long id);
}
