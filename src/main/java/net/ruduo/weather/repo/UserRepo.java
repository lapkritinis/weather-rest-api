package net.ruduo.weather.repo;

import net.ruduo.weather.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.UUID;


public interface UserRepo extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    void deleteUserById(UUID uuid);

    Optional<User> findFirstById(UUID uuid);

    Optional<User> findFirstByUsernameIgnoreCase(String username);
}
