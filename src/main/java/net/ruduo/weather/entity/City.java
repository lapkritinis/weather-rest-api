package net.ruduo.weather.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class City {
    @Id
    private long id;
    @Column(unique = true)
    private String name;
    private String country;
    private long area;
    private long population;
    @ElementCollection
    private Map<String, Double> coord;
}
