package net.ruduo.weather.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.ruduo.weather.model.UserDeserializer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Valid
@JsonDeserialize(using = UserDeserializer.class)
public class User {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID id;
    @Column(unique = true)
    @NotNull(message = "Username is required for user creation.")
    @Size(min = 3, max = 255, message = "Username should not be less than 3 or more than 255 characters.")
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) // Only deserialize, so it's not revealed accidentally
    @NotNull
    @Size(min = 8, max = 255, message = "Password should not be less than 3 or more than 255 characters.")
    private String password;
    @NotNull(message = "First name is required for user creation.")
    private String firstName;
    @NotNull(message = "Last name is required for user creation.")
    private String lastName;
    private String homeLocation;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "city_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY) // it's being set via custom deserializer
    private Set<City> favoriteCities = new HashSet<>();

    public User(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + "[PROTECTED]" + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", homeLocation='" + homeLocation + '\'' +
                ", favoriteCities=" + favoriteCities +
                '}';
    }
}
