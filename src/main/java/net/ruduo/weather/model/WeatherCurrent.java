package net.ruduo.weather.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class WeatherCurrent {
    BigDecimal temperatureMin;
    BigDecimal temperatureMax;
    Set<WeatherConditions> conditions = new HashSet<>();

    public WeatherCurrent() {
        this.temperatureMin = BigDecimal.valueOf(-30 + Math.random() * 60).setScale(1, BigDecimal.ROUND_HALF_UP);
        this.temperatureMax = temperatureMin.add(BigDecimal.valueOf(Math.random() * 10).setScale(1, BigDecimal.ROUND_HALF_UP));
        this.conditions.add(WeatherConditions.RAINY); // could be random
        this.conditions.add(WeatherConditions.FOG); // could be random
    }

    enum WeatherConditions {
        SUNNY, RAINY, SNOWY, FOG
    }
}
