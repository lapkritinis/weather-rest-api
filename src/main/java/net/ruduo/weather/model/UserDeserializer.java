package net.ruduo.weather.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import net.ruduo.weather.entity.City;
import net.ruduo.weather.entity.User;
import net.ruduo.weather.web.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@JsonComponent
public class UserDeserializer extends JsonDeserializer<User> {
    private final CityService cityService;

    @Autowired
    public UserDeserializer(CityService cityService) {
        this.cityService = cityService;
    }

    // Custom deserializer to support mapping from String to City upon user creation.

    @Override
    public User deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        User newUser = new User();

        // Setting fields using reflection
        Class<?> userClass = newUser.getClass();
        Field[] declaredFields = userClass.getDeclaredFields();
        Arrays.stream(declaredFields)
                .filter(field ->
                        !field.getName().equals("favoriteCities")
                )
                .forEach(field -> {
                    field.setAccessible(true);
                    setField(node, newUser, field);
                });

        // Setting favorite cities after validating that City does exists and populating it with values
        JsonNode favoriteCities = node.get("favoriteCities");
        Set<City> cities = new HashSet<>();

        if (favoriteCities != null && favoriteCities.isArray()) {
            for (final JsonNode city : favoriteCities) {
                City foundCity = cityService.getCityIdFromJson(city.asText());
                cities.add(foundCity);
            }
        }
        newUser.setFavoriteCities(cities);

        return newUser;
    }

    private void setField(JsonNode node, User newUser, Field field) {
        if (node.has(field.getName())) {
            try {
                if (field.getName().equals("id")) {
                    field.set(newUser, UUID.fromString(node.get(field.getName()).asText()));
                } else {
                    field.set(newUser, node.get(field.getName()).asText());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
