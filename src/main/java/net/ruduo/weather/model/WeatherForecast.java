package net.ruduo.weather.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class WeatherForecast {
    LocalDate date;
    LocalDateTime time;
    // temperatures, weather conditions, pressure, humidity
    BigDecimal temperatureMin;
    BigDecimal temperatureMax;
    // Weather Conditions
    BigDecimal pressure;
    BigDecimal humidity;

    private WeatherForecast() {
        this.temperatureMin = getNumberBetween(-30, 60);
        this.temperatureMax = getNumberBetween(temperatureMin.longValue(), 10);
        this.pressure = getNumberBetween(720, 80);
        this.humidity = getNumberBetween(0, 100);
    }

    public WeatherForecast(LocalDate date) {
        this();
        this.date = date;
    }

    public WeatherForecast(LocalDateTime time) {
        this();
        this.date = LocalDate.now();
        this.time = time;
    }

    private BigDecimal getNumberBetween(long min, long range) {
        return BigDecimal.valueOf(min + Math.random() * range).setScale(1, BigDecimal.ROUND_HALF_UP);
    }
}
